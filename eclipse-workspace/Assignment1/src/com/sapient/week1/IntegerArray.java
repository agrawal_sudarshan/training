package com.sapient.week1;

import java.util.Arrays;
import java.util.Scanner;

public class IntegerArray {
	private int size = 10;
	int[] array = new int[size];
	public Scanner sc = new Scanner(System.in);
	IntegerArray() {
		this.size = 10;
	}
	IntegerArray(int size){
		this.size = size;
	}
	IntegerArray(int[] array){
		this.array = array;
	}
	IntegerArray(IntegerArray arr){
		this.size = arr.size;
		this.array = arr.array;	
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int[] getArray() {
		return array;
	}

	public void setArray(int[] array) {
		this.array = array;
	}
	public void display()
	{
		for(int i=0; i<size;i++)
		{
			System.out.println(array[i]);
		}
	}
	public void reading() {
	
		for(int i=0; i<size;i++)
		{
			array[i] = sc.nextInt();
		}
	}
	public void sort()
	{
		Arrays.sort(array);
		display();
	}
	public void search() {
		System.out.println(Arrays.binarySearch(array, sc.nextInt()));
	}
	public void average() {
		int sum=0;
		for(int i : array) {
			sum+=array[i];
		}
		System.out.println(sum/size);
	}
	public static void main(String[] args) {
		IntegerArray ob = new IntegerArray();
		System.out.println("Enter the values of the array");
		ob.reading();
		System.out.println("Displaying the array");
		ob.display();
		System.out.println("Sorting the array ");
		ob.sort();
		System.out.println("Searching the array");
		ob.search();
		System.out.println("Calculating the average");
		ob.average();
	}
}
