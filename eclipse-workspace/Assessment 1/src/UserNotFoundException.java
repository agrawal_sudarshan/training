
public class UserNotFoundException extends Exception{
	public UserNotFoundException(){
		super("User id does not exist");
	}
	public UserNotFoundException(String x){
		super(x);
	}
}
