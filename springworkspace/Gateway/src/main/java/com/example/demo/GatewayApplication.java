package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import com.example.demo.filter.ErrorFilter;
import com.example.demo.filter.PostFilter;
import com.example.demo.filter.PreFilter;
import com.example.demo.filter.RouteFilter;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

	@Bean
	public PreFilter prefilter() {
		return new PreFilter();
	}
	@Bean
	public PostFilter postfilter() {
		return new PostFilter();
	}
	@Bean
	public RouteFilter routefilter() {
		return new RouteFilter();
	}
	@Bean
	public ErrorFilter errorfilter() {
		return new ErrorFilter();
	}
}
