package aspects;

import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class AopDemo {
@After("execution(* *.*(String))")
public void check1(JoinPoint j) {
	System.out.print(j.getSignature()+"\t");
	for(Object x : j.getArgs()) {
		String s =(String)x;
		System.out.print(s+"\t");
	}
	System.out.println(LocalDateTime.now());
	}

}
