package container;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import p1.Test1;
import p2.Test2;

@Configuration
public class JavaContainer {
@Bean
public Test1 call1() {
	return new Test1();
}
@Bean
public Test2 call2() {
	return new Test2();
}

}
