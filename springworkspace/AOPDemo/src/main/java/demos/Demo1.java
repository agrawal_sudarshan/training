package demos;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import p1.Test1;
import p2.Test2;

public class Demo1 {
public static void main(String[] args) {
	ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
	Test1 ob;
	Test2 ob2;
	ob=(Test1)context.getBean("test1");
	ob2=(Test2)context.getBean("test2");
	ob.m1("Hi");ob.m2("Hello");ob2.p1("Hey");ob2.p2("Yo");
}
}
