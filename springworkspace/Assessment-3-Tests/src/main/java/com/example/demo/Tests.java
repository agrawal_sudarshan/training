package com.example.demo;

import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

class Read {
	public static Scanner sc = new Scanner(System.in);
}

public class Tests {
	String URL_CREATE_EMPLOYEE;
	RestTemplate restTemplate;
	HttpHeaders headers;
	@Test
	public void testfordisplay() {
		URL_CREATE_EMPLOYEE = "http://localhost:8098/display";
	/*	headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);
		restTemplate = new RestTemplate();
		HttpEntity<CourseBean[]> entity = new HttpEntity<CourseBean[]>(headers); // RestTemplate
	*/	restTemplate = new RestTemplate();
		CourseBean[] result = restTemplate.getForObject(URL_CREATE_EMPLOYEE, CourseBean[].class);
	//	ResponseEntity<CourseBean[]> response =restTemplate.exchange(URL_CREATE_EMPLOYEE,HttpMethod.GET, entity,CourseBean[].class);
	//	CourseBean[] result = response.getBody();
		CourseBean ob = new CourseBean(1,"Course1","22/7/19","28/7/19","1000");
		Assert.assertEquals(ob.toString(), result[0].toString());
	}
	@Test
	public void testforadd() {
		URL_CREATE_EMPLOYEE = "http://localhost:8098/register";
		CourseBean ob = new CourseBean(3,"Course2","25/7/19","31/7/19","2000");
		 headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);
		restTemplate = new RestTemplate();
		HttpEntity<CourseBean> entity = new HttpEntity<>(ob,headers); // RestTemplate
		ResponseEntity<String> e = restTemplate.exchange(URL_CREATE_EMPLOYEE, HttpMethod.PUT,entity,String.class);
		String s = e.getBody();
		Assert.assertEquals("Course Added", s);
	}
	@Test
	public void testfordelete() {
		URL_CREATE_EMPLOYEE = "http://localhost:8098/delete";
		int id = 2;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Integer> entity = new HttpEntity<>(id,headers); // RestTemplate
		ResponseEntity<String> response =restTemplate.exchange(URL_CREATE_EMPLOYEE,HttpMethod.DELETE, entity,String.class);
		String result = response.getBody();
		Assert.assertEquals("Course Deleted", result);
	}
	@Test
	public void testforfind() {
		URL_CREATE_EMPLOYEE = "http://localhost:8098/find";
		int id = 1;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Integer> entity = new HttpEntity<>(id,headers); // RestTemplate
		ResponseEntity<CourseBean> response =restTemplate.exchange(URL_CREATE_EMPLOYEE,HttpMethod.POST, entity,CourseBean.class);
		CourseBean result = response.getBody();
		CourseBean ob = new CourseBean(1,"Course1","22/7/19","28/7/19","1000");
		Assert.assertEquals(ob.toString(), result.toString());
	}
}