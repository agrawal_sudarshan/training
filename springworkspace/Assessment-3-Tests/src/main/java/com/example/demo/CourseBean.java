package com.example.demo;

public class CourseBean {
private int id;
private String title;
private String sdate;
private String edate;
private String fees;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getSdate() {
	return sdate;
}
public void setSdate(String sdate) {
	this.sdate = sdate;
}
public String getEdate() {
	return edate;
}
public void setEdate(String edate) {
	this.edate = edate;
}
public String getFees() {
	return fees;
}
public void setFees(String fees) {
	this.fees = fees;
}
public CourseBean(int id, String title, String sdate, String edate, String fees) {
	super();
	this.id = id;
	this.title = title;
	this.sdate = sdate;
	this.edate = edate;
	this.fees = fees;
}
@Override
public String toString() {
	return "CourseBean [id=" + id + ", title=" + title + ", sdate=" + sdate + ", edate=" + edate + ", fees=" + fees
			+ "]";
}
public CourseBean() {
	super();
}

}
