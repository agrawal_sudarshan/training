package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assessment3TestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assessment3TestsApplication.class, args);
	}

}
