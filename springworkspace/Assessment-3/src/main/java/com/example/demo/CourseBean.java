package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CourseBean {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	private int id;
	@Column
	private String title;
	@Column
	private String sdate;
	@Column
	private String edate;
	@Override
	public String toString() {
		return "CourseBean [id=" + id + ", title=" + title + ", sdate=" + sdate + ", edate=" + edate + ", fees=" + fees
				+ "]";
	}
	@Column
	private String fees;
public CourseBean(int id, String title, String sdate, String edate, String fees) {
		super();
		this.id = id;
		this.title = title;
		this.sdate = sdate;
		this.edate = edate;
		this.fees = fees;
	}
public String getFees() {
		return fees;
	}
	public void setFees(String fees) {
		this.fees = fees;
	}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getSdate() {
	return sdate;
}
public void setSdate(String sdate) {
	this.sdate = sdate;
}
public String getEdate() {
	return edate;
}
public void setEdate(String edate) {
	this.edate = edate;
}
public CourseBean() {
	super();
}

}
