package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	CourseDAO dao;
	@GetMapping("/display")
	public List<CourseBean> display(){
		return (List<CourseBean>)dao.findAll();
	}
	@PutMapping("/register")
	public String add(@RequestBody CourseBean ob){
		dao.save(ob);
		return "Course Added";
	}
	@DeleteMapping("/delete")
	public String delete(@RequestBody int id){
		dao.deleteById(id);
		return "Course Deleted";
	}
	@PostMapping("/find")
	public Optional<CourseBean> find(@RequestBody int id){
		Optional<CourseBean> ob =dao.findById(id);
		return ob;
	}
}
