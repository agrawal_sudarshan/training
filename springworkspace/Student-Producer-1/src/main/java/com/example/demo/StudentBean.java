package com.example.demo;

public class StudentBean {
private String id;
private String fname;
private String lname;
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getFname() {
	return fname;
}
public void setFname(String fname) {
	this.fname = fname;
}
public String getLname() {
	return lname;
}
public void setLname(String lname) {
	this.lname = lname;
}
public StudentBean(String id, String fname, String lname) {
	super();
	this.id = id;
	this.fname = fname;
	this.lname = lname;
}
@Override
public String toString() {
	return "StudentBean [id=" + id + ", fname=" + fname + ", lname=" + lname + "]";
}

}
