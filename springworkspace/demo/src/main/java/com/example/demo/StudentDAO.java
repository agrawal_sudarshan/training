package com.example.demo;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDAO {
	@Autowired
	StudentList l1;
	@Autowired
	StudentList l2;

	public List<StudentBean> getStudents2() {
		l2.getList().add(new StudentBean("CHinmay", "25", "Nashik"));
		return l2.getList();
	}

	public List<StudentBean> getStudents() {
		return l1.getList();
	}

	public List<StudentBean> getDetails(String name) {
		return l1.getList().stream().filter(e -> e.getName().equals(name)).collect(Collectors.toList());
	}

	public String insert(StudentBean ob) {
		l1.getList().add(ob);
		return "Added Successfully";
	}
	public String delete(String name) {
		l1.getList().removeIf(e->e.getName().equals(name));
		return "Deleted Successfully";
	}

	public String update(String name,StudentBean bean) {
		l1.getList().stream().filter(e->e.getName().equals(name)).forEach(e->{e.setName(bean.getName());e.setAge(bean.getAge());e.setCity(bean.getCity());});
		return "UPDATED SUCCESSFULLY";
	}

}
