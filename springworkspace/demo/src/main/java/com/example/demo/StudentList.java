package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class StudentList {
private List<StudentBean> list;
public StudentList() {
	super();
	list = new ArrayList<>();
	list.add(new StudentBean("Sudarshan","21","Pune"));
	list.add(new StudentBean("Sachin","22","Mumbai"));
	list.add(new StudentBean("Tejas","23","Gurgaon"));
	list.add(new StudentBean("Shrirang","24","Delhi"));
}

public List<StudentBean> getList() {
	return list;
}

public void setList(List<StudentBean> list) {
	this.list = list;
}


}
