package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class StudentDetails {
	@Id 
	private String regno;
	@Column
	private String fname;
	@Column
	private String lname;
	@Column
	private String dob;
	@Column
	private String citycode;
	@Transient
	
	Set<StudentMarks> sm = new HashSet<>();
	
	public String getRegno() {
		return regno;
	}
	public void setRegno(String regno) {
		this.regno = regno;
	}
	public Set<StudentMarks> getSm() {
		return sm;
	}
	@OneToMany(mappedBy = "regno")
	public void setSm(Set<StudentMarks> sm) {
		this.sm = sm;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getCitycode() {
		return citycode;
	}
	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}
	public StudentDetails(String id, String fname, String lname, String dob, String citycode) {
		super();
		this.regno = id;
		this.fname = fname;
		this.lname = lname;
		this.dob = dob;
		this.citycode = citycode;
	}
	public StudentDetails() {
		super();
	}
	
}
