package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Demo2 {
public static void main(String[] args) {
	ApplicationContext context = new AnnotationConfigApplicationContext(JavaContainer.class);
	ListOfHolidays ob;
	ob=context.getBean(ListOfHolidays.class);
	System.out.println(ob);
}
}
