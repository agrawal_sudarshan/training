package p1;

public class Holiday {
private String date;
private String hname;
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getHname() {
	return hname;
}
public void setHname(String hname) {
	this.hname = hname;
}
public Holiday(String date, String hname) {
	super();
	this.date = date;
	this.hname = hname;
}
@Override
public String toString() {
	return "Holiday [date=" + date + ", hname=" + hname + "]";
}

}
