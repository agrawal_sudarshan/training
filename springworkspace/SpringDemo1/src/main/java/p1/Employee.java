package p1;

public class Employee {
private int age;
private String name;
private String city;
public Employee(String name,String city) {
	super();
	this.city = city;
	this.name = name;
}

public Employee(int age, String name) {
	super();
	this.age = age;
	this.name = name;
}

@Override
public String toString() {
	return "Employee [age=" + age + ", name=" + name + ", city=" + city + "]";
}

public Employee() {
	super();
}

}
