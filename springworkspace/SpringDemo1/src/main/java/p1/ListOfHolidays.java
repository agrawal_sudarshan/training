package p1;

import java.util.HashSet;
//import java.util.ArrayList;
//import java.util.List;
import java.util.Set;

public class ListOfHolidays {
private Set<Holiday> holidays = new HashSet<Holiday>();

public Set<Holiday> getHolidays() {
	return holidays;
}

public void setHolidays(Set<Holiday> holidays) {
	this.holidays = holidays;
}

@Override
public String toString() {
	return "ListOfHolidays [holidays=" + holidays + "]";
}

}
