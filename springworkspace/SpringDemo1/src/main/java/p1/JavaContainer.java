package p1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {
	@Bean
	@Scope("singleton")
	public Hello get1() {
	return new Hello();
}
	@Bean
	public Employee get2() {
		Employee ob;
		ob=new Employee(12,"Sudarshan");
		return ob;
	}
	@Bean
	public ListOfHolidays get3() {
		ListOfHolidays ob;
		ob = new ListOfHolidays();
		ob.getHolidays().add(new Holiday("26/1/2019","Republic day"));
		ob.getHolidays().add(new Holiday("15/8/2019","Independence day"));
		return ob;
	}
}
