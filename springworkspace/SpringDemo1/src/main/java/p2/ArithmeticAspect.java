package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0)
public class ArithmeticAspect {
	
	@Before("execution( * *.*(double,double))")
public void check1(JoinPoint jpoint) {
	for(Object x : jpoint.getArgs()) {
		double v =(Double)x;
		if(v<0) {
			throw new IllegalArgumentException("Number cannot be negative");
		}
	}
}
	
	@AfterReturning(pointcut  = "execution( * *.*(double,double))" , returning = "returnval")
public void check2(JoinPoint jpoint, Object returnval) {
		double v =(Double)returnval;
		if(v<0) {
			throw new IllegalArgumentException("Result cannot be negative");
		}
}
	
	@AfterReturning(pointcut  = "execution( * *.*(double,double))" , returning = "returnval")
	public void check4(JoinPoint jpoint, Object returnval) {
			double v =(Double)returnval;
			if(v>1000) {
				throw new IllegalArgumentException("Result cannot cross 1000");
			}
	}
	@Before("execution( * *.div(double,double))")
public void check3(JoinPoint jpoint) {
	for(Object x : jpoint.getArgs()) {
		double v =(Double)x;
		if(v<0) {
			throw new IllegalArgumentException("Number cannot be negative");
		}
		if(v==0) {
			throw new IllegalArgumentException("Division by zero not possible");
		}
	}
}

}
