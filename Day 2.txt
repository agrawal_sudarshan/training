1. Implement Inheritance, Abstraction, Polymorphism and Encapsulation with examples
2. Constructors and disposing techniques.

How to change compiler for a project:
                1. Right click on project and go to Build Path -> Configure Build Path...
                2. Go to Libraries tab

Eclipse
                File->Switch Workspace->Other

                Package is also called namespace
                It is used for naming purpose

                Why IDE?
                                To generate the code

                Access modifiers
                                private -> Same class
                                default -> Current class
                                protected -> available in current package and outside via inheritance (not via instantiation)

                Encapsulation
                                Hiding data using access modifiers.
                                POJO/Java Bean/Value object class only has private data members and their getters/setters.

                Polymorphism
                                Static/Compile time binding
                                Method Signature:
                                                Name of the method
                                                Number of arguments
                                                Type of arguments
                                                Order of the arguments
                                                Return type is not a part of signature.

                                Super class reference and sub class object is Runtime Polymorphism

Assignment:
                Question 1:
                Create a class IntegerArray which will have following methods/constructors
                if the array size is not specified default size should be 10
                if the size is specified, array size should be according to the size
                it should be able to adopt another array
                it should have a copy constructor
                it should have a method for 
                                1. reading
                                2. display
                                3. sort
                                4. find average
                                5. to do a search

                Question 2:
                To Add, Multiply, Subtract two matrices.
                if size is not given then 3x3, if given then size accordingly.
                it should be able to adopt another matrix

                Question 3:
                Accept amount in figures then convert it to words

		Question 4 : Undone
		open an image file, convert the colored image to b&w

		Question 5 : 
		Create a class to check if a number is prime or not and design unit test cases.

HOMEWORK : 
1. Resource bundling 
2. Logger4j
3. lambda expression - method reference
	- streams 
	- filters 
	- mapping 
	- statistical info
	- behaviour passing through a method
	- inbuilt functional interfaces 
4. Collections in java and data structures in C 	
5. Constructors and disposing techniques.