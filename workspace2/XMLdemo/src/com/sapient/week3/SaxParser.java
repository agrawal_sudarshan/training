package com.sapient.week3;
import org.xml.sax.SAXException;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class SaxParser extends DefaultHandler{

		boolean bname = false;
		boolean brole = false;
		
		public void startElement(String arg0, String arg1, String arg2, Attributes arg3) {
			
		if(arg2.equalsIgnoreCase("tns:name")) {
			bname=true;
		}
		if(arg2.equalsIgnoreCase("tns:role")) {
			brole=true;
		}
		}
		public void endElement(String arg0, String arg1, String arg2) throws SAXException {
		}
		
		public void characters(char[] arg0, int arg1, int arg2) {
			if(bname) {
				System.out.println("Name : " + new String(arg0,arg1,arg2));
			}
			if(brole) {
				System.out.println("Role : " + new String(arg0,arg1,arg2));
		}
		}
		
}