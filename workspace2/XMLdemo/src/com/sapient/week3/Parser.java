package com.sapient.week3;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Parser {
public static void main(String[] args) {
	DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
	DocumentBuilder builder;
	try {
		builder = documentBuilder.newDocumentBuilder();
		Document doc = builder.parse("C:\\Users\\sudagraw\\Desktop\\training\\workspace2\\XMLdemo\\WebContent\\NewXML.xml");
		NodeList list = doc.getElementsByTagName("tns:employee");
		for(int i=0;i<list.getLength();i++) {
			Element e = (Element)list.item(i);
			if(e.getNodeType()==Node.ELEMENT_NODE) {
				String id = e.getAttribute("id");
				String city = e.getAttribute("city");
				NodeList list2 = e.getChildNodes();
				System.out.println("Employee id - " + id + " City - " +city);
				for(int j=0;j<list2.getLength();j++) {
					System.out.println(list2.item(j).getNodeName() + " - " +list2.item(j).getTextContent());
				}
			}
		}
	} catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
