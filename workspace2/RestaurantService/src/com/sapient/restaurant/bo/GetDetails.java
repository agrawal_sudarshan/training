package com.sapient.restaurant.bo;
import java.net.URI;

import javax.ws.rs.client.Client;  
import javax.ws.rs.client.ClientBuilder;  
import javax.ws.rs.client.WebTarget;  
import javax.ws.rs.core.MediaType;  
import javax.ws.rs.core.UriBuilder;  
import org.glassfish.jersey.client.ClientConfig;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;


public class GetDetails {
public static void main(String[] args) throws Exception {
	
	ClientConfig config = new ClientConfig();  
    Client client = ClientBuilder.newClient(config);  
    WebTarget target = client.target(getBaseUri());  
    //Now printing the server code of different media type  
  // System.out.println(target.path("rest").path("hello").request().accept(MediaType.TEXT_PLAIN).get(String.class));  

    //System.out.println(target.path("rest").path("hello").request().accept(MediaType.TEXT_HTML).get(String.class)); 
   // System.out.println(target.path("rest").path("hello").request().accept(MediaType.TEXT_XML).get(String.class));
    String txt = target.path("geocode")
    		.queryParam("lat",28.4595)
    		.queryParam("lon",77.0266)
    		.request()
    		.header("user-key","d6212ac5ba3b88941dc763c83bc3e013")
    		.accept(MediaType.TEXT_PLAIN).
    		get(String.class);
    
    Object obj=JSONValue.parse(txt);  
    JSONObject jsonObject = (JSONObject) obj;
    JSONArray object = (JSONArray)jsonObject.get("nearby_restaurants");
    System.out.println(object);
   // for(i in object.restaurant)

}

private static URI getBaseUri() {
	// TODO Auto-generated method stub
	 return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1").build();
	
}

}
