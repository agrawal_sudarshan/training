package com.sapient.model;

import java.sql.*;
import java.util.List;
public class UserDAO {
	ResultSet rs = null;
	List<StudentBean> al=null;
	public String login(String username, String pwd) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;
			ps = con.prepareStatement("select username,password,type from users where username=? AND password=?");
			ps.setString(1, username);
			ps.setString(2, pwd);
			rs = ps.executeQuery();
			if(rs.next()){
				return rs.getString(3);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}return "Invalid";
		
	}
	
	
	public void insertStudent(UserBean ub, StudentBean sb) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("insert into students(id,name,rollno,percent) values(?,?,?,?)");
			ps.setInt(1, sb.getId());
			ps.setString(2,sb.getName());
			ps.setInt(3,sb.getRollno());
			ps.setFloat(4, sb.getPercent());
			int i = ps.executeUpdate();
			System.out.println(i);
			if(i>0) {
				System.out.println("Inserted into student");
			}
			else {
				System.out.println("not inserted into student");
			}
			
			PreparedStatement ps1 = con.prepareStatement("insert into users(id,username,password,type) values(?,?,?,?)");
			ps1.setInt(1, ub.getId());
			ps1.setString(2, ub.getUsername());
			ps1.setString(3, ub.getPassword());
			ps1.setString(4, ub.getType());
			int o = ps1.executeUpdate();
			System.out.println(o);
			if(o>0) {
				System.out.println("Inserted into user ");
			}
			else {
				System.out.println("not inserted into user");
			}
			con.commit();
		} catch (Exception e) {
		}
		
		
	}
	
	
	
	

}
